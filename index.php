<?php

require_once __DIR__.'/bootstrap.php';

// Sample data
$foo = [
  [ 'name'          => 'Alice' ],
  [ 'name'          => 'Bob' ],
  [ 'name'          => 'Eve' ],
];

$miTemplate = "Esta es mi frase desde el archivo index.php";

// Render our view
$template = $twig->load('index.html.twig');

echo $template->render([
    'frase' => $miTemplate,
    'foo' => $foo
]);